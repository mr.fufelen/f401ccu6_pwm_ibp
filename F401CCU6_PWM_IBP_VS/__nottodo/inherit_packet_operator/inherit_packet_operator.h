#ifndef INHERIT_PACKET_OPERATOR_H
#define INHERIT_PACKET_OPERATOR_H

//#include <_stdint.h>

//#include <Arduino.h>
//#include <inherit_packet_operator/packet_operator/packet_operator.h>
#include "packet_operator/packet_operator.h"

//#include <SoftwareSerial.h>

class uartPacketOperator : public PacketOperator
{ // создан для хранения буферов и указателей на обьекты com портов
public:
    //uartPacketOperator(char header, char inbetween, char terminator, SoftwareSerial &SerialPtr); todo
	uartPacketOperator(char header, char inbetween, char terminator);//, SoftwareSerial &SerialPtr);

    void add_COM_to_check(); // если аппаратный UART
    //void add_COM_to_check(SoftwareSerial &uart_com); todo

    void check_UARTs(); // проверка всех переданных на управление последовательных портов

protected:
    // глобальные переменные

    //SoftwareSerial *SerialPtr; // указатель на программный COM Port todo

    /*
        // не реализованная идея - нет нужды (пока что)
        SoftwareSerial **ptr_SoftwareSerial = nullptr; // указатель на массив добавленных последовательных портов
    */

    uint8_t counter_coms = 0;

    uint8_t hard_buf_counter = 0; // размер полученного пакета
    uint8_t soft_buf_counter = 0; // размер полученного пакета // volatile - так как прием

    const uint8_t sizeof_mesg_buf = 255; // если не будет хватать ram - сделать меньше (>=30)
    char hard_buf[255];                  // строка-буфер с полученной посылкой для аппаратного Uarta
    char soft_buf[255];                  // строка-буфер с полученной посылкой для программного Uarta
};

#endif // INHERIT_PACKET_OPERATOR_H