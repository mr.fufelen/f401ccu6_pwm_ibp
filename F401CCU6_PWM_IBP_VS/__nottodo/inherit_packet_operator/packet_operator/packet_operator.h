#ifndef PACKET_OPERATOR_H
#define PACKET_OPERATOR_H


#include "main.h"

//#include <Arduino.h>

class PacketOperator
{
public:
    struct main_param_packet
    { // хранятся ключевые символы для парсинга пакета
        char mpp_header;
        char mpp_inbetweenchar;
        char mpp_terminator_pos;

        // массив команд

        // char **ptr_to_allocated_commas; // хранит указатель на выделенную память в которой хранятся указатели на строковые команды
        // char *ptr_to_allocated_commas; // хранит указатель на выделенную память в которой хранятся указатели на строковые команды

        // выделить в памяти массив под необходимое количество команд
        // под каждую команду память выделять отдельно в зависимости от ее размера
    };
    main_param_packet main_param;

    PacketOperator(){};                                           // для наследников
    PacketOperator(char header, char inbetween, char terminator); // добавить прием ключевых символов для парсинга пакета

    void deparse_packet(char *mesg_buf, uint8_t *counter);

    void add_comma(void (*func)(), char const *ptr_mass_char);
    void add_comma(void (*func)(uint32_t), char const *ptr_mass_char);

    void help();

    void do_attached_func();
    void do_func();

    void cout_struct_with_key_pos();

protected:
    struct key_positions
    {                                 // содержит ключевые данные по полученному пакету
        uint8_t ks_header_pos;        // позиция заголовка
        uint8_t ks_command_pos;       // позиция начала команды
        uint8_t ks_inbetweenchar_pos; // позиция разделителя команды и числа
        uint8_t ks_value_pos;         // позиция начала числа в команде
        uint8_t ks_terminator_pos;    // позиция терминатора
        uint32_t ks_data_comma;       // число пришедшее с командой
        uint8_t num_comma;            // номер команды из enum в пакете
        bool comma_finded;
    };
    key_positions struct_with_key_pos; // обьект структуры (в конструкторе все поля инициализируются нулями)

    char const **storage_comma_pointers; // хранение указателей на команды
    // uint16_t **storage_attached_func_pointers = nullptr; // хранение указателей
    void (**storage_attached_func_pointers)(uint32_t); // хранение указателей на функции
    uint8_t counter_commands;                          // счетчик количества команд

    void fill_struct_with_key_positions(char *mesg_buf, uint8_t *counter); // заполнение управляющей структуры ключевыми позициями
    uint32_t extract_data_from_packet(char *mesg_buf);
    void spot_command_in_package(char *mesg_buf);

    void cout_commas();

    bool attach_func(void (*func)());         // принять указатель на функцию
    bool attach_func(void (*func)(uint32_t)); // принять указатель на функцию

    bool attach_comma(char const *ptr_mass_char);
};

#endif