#include "program.h"

#define STACK_SIZE_UART_HANDLER 256
//#include "serial_handler/HardwareSerial.h"

//#include "serial_handler/Print.h"



bool data_transmitted = false;

void uart_handler(void * pvParameters);

void my_main();
extern "C"
{
	void program()
	{
		my_main();
	}
}

uint8_t data_uart[3];
TaskHandle_t uart_handler_handle;

void my_main()
{	
	xTaskCreate(uart_handler, "uart_handler", STACK_SIZE_UART_HANDLER, NULL, 1, &uart_handler_handle);
}

QueueHandle_t uart_queue;
Serial mySerial(&huart1);

void uart_handler(void * pvParameters)
{
	uart_queue = xQueueCreate(100, sizeof(uint8_t));
	
	HAL_UART_Receive_IT(&huart1, (uint8_t*)data_uart, 1);
	
	mySerial.println("startup_uart_handler");
	
	
	for (;;)
	{	
		uint8_t data_uart = mySerial.read();
		
		//mySerial.print(data_uart);
		
		mySerial.write(data_uart);
		
		//mySerial.print("byte_received");	
		//vTaskDelay(pdMS_TO_TICKS(100));
		
		
		HAL_GPIO_TogglePin(BLUE_LED_GPIO_Port, BLUE_LED_Pin);
	}
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) 
{
	if (huart == &huart1)
	{
		BaseType_t xHigherPriorityTaskWoken;
		xHigherPriorityTaskWoken = pdFALSE;
		
		xQueueSendFromISR(uart_queue, data_uart, &xHigherPriorityTaskWoken);
		
		HAL_UART_Receive_IT(&huart1, (uint8_t*)data_uart, 1);
	}
}





