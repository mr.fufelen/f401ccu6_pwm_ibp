#pragma once
#include "Print/Print.h"
#include "main.h"

#include "cmsis_os.h"
#include "message_buffer.h"

#include <stdlib.h>
#include "../program.h"

#define RECEIVE_BUFFER_SIZE 100
#define UART_MAX_COMMANDS 10

class Serial : public Print
{
public:
	Serial(UART_HandleTypeDef *, DMA_HandleTypeDef *);
	Serial(UART_HandleTypeDef *_uart);
		
	virtual size_t write(uint8_t) override final; 
	virtual size_t write(const uint8_t *buffer, size_t size) override final;
	void serial_stream_transmit();
	void serial_stream_receive();
	void serial_LED_control();
	void lock();
	void unlock();
	void rx_irq();
	void tx_notifyMeFromISR();
	void error_notifyMeFromISR();
	
	uint8_t read();
	
	
protected:
	bool compareWithConstChar(char*, const char*);
	int getNumberFromCommand(char*);
	char *uart_transmit_buf;
	char *execute_receive_buf;
	char rx_cbuf;
	char rx_buf[RECEIVE_BUFFER_SIZE];
	int rx_pos = 0;
	MessageBufferHandle_t serial_transmit_mes;
	MessageBufferHandle_t serial_receive_mes;
	MessageBufferHandle_t *led_control_message;
	SemaphoreHandle_t serial_mutx;
	TaskHandle_t uart_transmit_handle;
	TaskHandle_t uart_receive_handle;
	
	UART_HandleTypeDef *uart;
	DMA_HandleTypeDef *dma;
	
	bool is_rx_error = false;
	bool is_rx_complete = false;
};