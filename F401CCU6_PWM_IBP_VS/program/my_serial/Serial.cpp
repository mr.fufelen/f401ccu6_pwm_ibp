#include "Serial.h"



//#include "ld_pwm.h"
//#include "SerialCommandHandler.h"
//
//#include "Regs.h"

//#include "Program.h"

//extern SerialCommandHandler commander;

#define UART_STREAM_RATE 5	//ms
#define SERIAL_TX_DELAY portMAX_DELAY
#define SERIAL_RX_DELAY portMAX_DELAY
#define EXECUTE_RECEIVE_DELAY portMAX_DELAY
#define HAL_UART_DELAY 1000

#define RTOS_UART_RX_STACK_SIZE 256
#define RTOS_UART_RX_PRIORITY 2

#define RTOS_UART_TX_STACK_SIZE 256
#define RTOS_UART_TX_PRIORITY 2

#define RTOS_LED_CONTROL_HEAP_SIZE 256

#define MESSAGE_BUFFER_SIZE 200

#define UART_RECEIVE_TIMEOUT 5
#define UART_RECEIVE_NOTIF_TIMEOUT 100

void serial_stream_transmit_static(void * pvParameters);
void serial_stream_receive_static(void * pvParameters);
void serial_LED_control_static(void * pvParameters);

void helloworld(Serial *stream);


// определенные виртуальные от print
size_t Serial::write(uint8_t value) {
	//xMessageBufferSend(serial_transmit_mes, &value, sizeof(value), pdMS_TO_TICKS(SERIAL_TX_DELAY));
	//xMessageBufferSend(serial_transmit_mes, &value, sizeof(value), SERIAL_TX_DELAY);
	
	while (HAL_UART_Transmit_IT(uart, (uint8_t *)&value, sizeof(value)) != HAL_OK) ;
	return 1;
}

size_t Serial::write(const uint8_t *buffer, size_t size)
{
	//xMessageBufferSend(serial_transmit_mes, buffer, size, pdMS_TO_TICKS(SERIAL_TX_DELAY));
	//xMessageBufferSend(serial_transmit_mes, buffer, size, SERIAL_TX_DELAY);
	
	while (HAL_UART_Transmit_IT(uart, (uint8_t *)buffer, size) != HAL_OK) ;
	return 1;
}

Serial::Serial(UART_HandleTypeDef *_uart)
{
	uart = _uart;
}

uint8_t Serial::read()
{// отдает очередной байт из буфера последовательного порта
	
	uint8_t uart_byte = NULL;
	xQueueReceive(uart_queue, &uart_byte, portMAX_DELAY);
	return uart_byte;
	
//	if (xQueueReceive(uart_queue, &uart_byte, portMAX_DELAY) == pdTRUE)
//		return uart_byte;
//	else
//		return pdFALSE;
}


Serial::Serial(UART_HandleTypeDef *_uart, DMA_HandleTypeDef *_dma)
{
	uart = _uart;
	dma = _dma;
	//serial_mutx = xSemaphoreCreateRecursiveMutex();
	serial_transmit_mes = xMessageBufferCreate(MESSAGE_BUFFER_SIZE);
	serial_receive_mes = xMessageBufferCreate(RECEIVE_BUFFER_SIZE);
	uart_transmit_buf = new char[MESSAGE_BUFFER_SIZE];
	
	//	execute_receive_buf = new char[RECEIVE_BUFFER_SIZE];
	xTaskCreate(serial_stream_transmit_static, "UART_SEND", RTOS_UART_TX_STACK_SIZE, this, RTOS_UART_TX_PRIORITY, &uart_transmit_handle);
	xTaskCreate(serial_stream_receive_static, "UART_RECEIVE", RTOS_UART_RX_STACK_SIZE, this, RTOS_UART_RX_PRIORITY, &uart_receive_handle);
}



void Serial::rx_irq()
{
	if (rx_pos < RECEIVE_BUFFER_SIZE - 1)
	{
		if (rx_cbuf == '\r' || rx_cbuf == '\n')
		{
			if (rx_pos != 0)
			{
				rx_buf[rx_pos] = 0;
				rx_pos = 0;
				BaseType_t xHigherPriorityTaskWoken = pdFALSE;
				is_rx_complete = true;
				vTaskNotifyGiveFromISR(uart_receive_handle, &xHigherPriorityTaskWoken);
				portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
			}
		}
		else
		{
			rx_buf[rx_pos] = rx_cbuf;
			rx_pos++;
		}
	}
	else
	{
		rx_buf[rx_pos] = 0;
		rx_pos = 0;
		BaseType_t xHigherPriorityTaskWoken = pdFALSE;
		is_rx_complete = true;
		vTaskNotifyGiveFromISR(uart_receive_handle, &xHigherPriorityTaskWoken);
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
	HAL_UART_Receive_IT(uart, (uint8_t*)(&rx_cbuf), 1);
}

void Serial::tx_notifyMeFromISR()
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	vTaskNotifyGiveFromISR(uart_transmit_handle, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void Serial::error_notifyMeFromISR()
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	is_rx_error = true;
	vTaskNotifyGiveFromISR(uart_receive_handle, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

int Serial::getNumberFromCommand(char* inp)
{
	char buf[10];
	uint8_t i = 0, c = 0;
	bool toBuf = false;
	while (inp[i] != 0)
	{
		if (inp[i] == ' ')
		{
			toBuf = true;
		}
		else
		{
			if (toBuf)
			{
				buf[c] = inp[i];
				c++;
			}
		}
		i++;
	}
	buf[c] = 0;
	return atoi(buf);
}


void serial_stream_transmit_static(void * pvParameters)
{
	static_cast <Serial*> (pvParameters)->serial_stream_transmit();
}

void serial_stream_receive_static(void * pvParameters)
{
	static_cast <Serial*> (pvParameters)->serial_stream_receive();
}


bool Serial::compareWithConstChar(char* str1, const char* str2)
{
	if (str1[0] == 0)
	{
		return false;
	}
	else
	{
		int i = 0;
		while (str1[i] != 0 && str2[i] != 0)
		{
			if (str1[i] != str2[i])
			{
				return false;
			}
			i++;
		}
		return true;
	}
}

void Serial::serial_stream_transmit()
{
	for (;;)
	{
		size_t xReceivedBytes = xMessageBufferReceive(serial_transmit_mes, (void *) uart_transmit_buf, MESSAGE_BUFFER_SIZE, portMAX_DELAY);
		if (xReceivedBytes > 0)
		{
			//HAL_UART_Transmit_DMA(uart, (uint8_t*)uart_transmit_buf, (uint16_t)xReceivedBytes);
			HAL_UART_Transmit_IT(uart, (uint8_t*)uart_transmit_buf, (uint16_t)xReceivedBytes);
			ulTaskNotifyTake(pdTRUE, portMAX_DELAY); // будет висеть пока не получит нотификацию извне
		}
	}
}

void Serial::serial_stream_receive()
{
	bool resume_search = true;
	HAL_UART_Receive_IT(uart, (uint8_t*)(&rx_cbuf), 1);
	for (;;)
	{
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
		
		if (is_rx_complete)
		{
			//commander.parse(rx_buf);
			HAL_UART_Receive_IT(uart, (uint8_t*)(&rx_cbuf), 1);
			is_rx_complete = false;
			resume_search = true;
		}
		
		if (is_rx_error)
		{
			rx_pos = 0;
			HAL_UART_Receive_IT(uart, (uint8_t*)(&rx_cbuf), 1);
			is_rx_error = false;
		}
	}
}



void Serial::lock()
{
	xSemaphoreTake(serial_mutx, portMAX_DELAY);
}

void Serial::unlock()
{
	xSemaphoreGive(serial_mutx);
}

void helloworld(Serial* stream)
{
	stream->lock();
	stream->print("\n[UART COMMAND]\tHello world!\n\n");
	stream->unlock();
}