//#include <inherit_packet_operator/inherit_packet_operator.h>
//#include <inherit_packet_operator/packet_operator/packet_operator.h>

#include "inherit_packet_operator.h"
// #include "packet_operator/packet_operator.h"


//#include "program.h"
#include "../program/program.h"

extern QueueHandle_t uart_queue;

uartPacketOperator::uartPacketOperator(char header, char inbetween, char terminator)//, SoftwareSerial &_SerialPtr)
{
  //SerialPtr = &_SerialPtr; // взятие адреса и присвоение его хранителю на указатель

  // SerialPtr - указатель на обьект программного сериал
  storage_comma_pointers = nullptr;
  counter_commands = 0;

  // инициализация настроек для парсинга пакета
  main_param.mpp_header = header;
  main_param.mpp_inbetweenchar = inbetween;
  main_param.mpp_terminator_pos = terminator;

  struct_with_key_pos = {0}; // заполнение полей структуры нулями
}

void uartPacketOperator::check_UARTs()
{
    if(uxQueueMessagesWaiting(uart_queue)) // если очередь не пуста
  //if (Serial.available()) // заполнение аппаратного буфера
  {
	  
	  //		uint8_t uart_byte;
	  
    //char val = Serial.read(); // чтение из очереди
	char val;
	xQueueReceive(uart_queue, &val, portMAX_DELAY);

    hard_buf[hard_buf_counter] = val;
    hard_buf_counter++;

    if (val == main_param.mpp_terminator_pos)
    {
      deparse_packet(hard_buf, (uint8_t *)&hard_buf_counter);

      cout_struct_with_key_pos(); // вывести текушее состояние структуры разбитого пакета

      do_func();

      hard_buf_counter = 0;
    }
  }

//  if (SerialPtr->available()) // заполнение аппаратного буфера
//  {
//    char val = SerialPtr->read();
//    soft_buf[soft_buf_counter] = val;
//    soft_buf_counter++;
//
//    if (val == main_param.mpp_terminator_pos)
//    {
//      deparse_packet(soft_buf, (uint8_t *)&soft_buf_counter);
//
//      do_func();
//
//      soft_buf_counter = 0;
//    }
//  }

  /*
  // обработчик буфера для программного порта
      if (mySerial.available()) // заполнение программного буфера
{
  char val = mySerial.read();
  soft_buf[soft_buf_counter] = val;
  soft_buf_counter++;
  if (val == main_param.mpp_terminator_pos)
  {
    // добавить динамическое выделение?
    key_positions struct_with_key_pos; // обьект структуры
    deparse_packet(struct_with_key_pos, soft_buf, (uint8_t *)&soft_buf_counter);
    makeComma(struct_with_key_pos);
  }
}
  */
}

/*
void uartPacketOperator::add_COM_to_check(SoftwareSerial &uart_com)
{
  counter_coms++;

  SoftwareSerial **ptr_SoftwareSerial_new = nullptr; // промежуточный буфер

  ptr_SoftwareSerial = new SoftwareSerial *[counter_coms]; // counter_coms -  счетчик кол-ва добавленных команд

  memset(ptr_SoftwareSerial_new, 0, (sizeof(SoftwareSerial *) * counter_coms)); // заполнение нулями

  // memset(storage_pointers_new, 0, (sizeof(char const *) * counter_commands));   // заполнение нулями

  memmove(ptr_SoftwareSerial_new, ptr_SoftwareSerial, (sizeof(SoftwareSerial *) * counter_coms)); // перемещение указателей из основного буфера в промежуточный

  delete[] ptr_SoftwareSerial; // освобождение памяти основного буфера

  *(ptr_SoftwareSerial_new + (counter_coms - 1)) = &uart_com; // выбор ячейки и передача в нее указателя на команду

  ptr_SoftwareSerial = ptr_SoftwareSerial_new; // теперь промежуточный буфер - это основной буфер с командами
                                               // return true; // добавить проверку выделилась ли память?
};
*/