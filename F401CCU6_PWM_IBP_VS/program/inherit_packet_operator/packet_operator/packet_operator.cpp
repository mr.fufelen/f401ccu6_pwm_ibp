//#include <inherit_packet_operator/packet_operator/packet_operator.h>

#include <stdio.h>
#include <stdlib.h>

#include "../packet_operator/packet_operator.h"

//#define Serial mySerial

#include "../program/program.h"

extern Serial mySerial;//(&huart1);
Serial Serial = mySerial;

void PacketOperator::deparse_packet(char *mesg_buf, uint8_t *counter)
{

    fill_struct_with_key_positions(mesg_buf, counter); // выделить из сообщения ключевые точки

    spot_command_in_package(mesg_buf); // проверка на совпадение команды из списка массивов char

    if (struct_with_key_pos.comma_finded == true)
    {                                       //нет команды - не тратим время на выделение числа из посылки
        extract_data_from_packet(mesg_buf); // выделение числа из посылки
    }
    else
    {
         Serial.println("comma not finded");
        return;
    }
};

uint32_t PacketOperator::extract_data_from_packet(char *mesg_buf)
{
    if (struct_with_key_pos.ks_value_pos != 0) // выделение числа из команды
    {
        // value_from_comma = atoi(&mesg_buf[value_pos]);

        //uint32_t value_from_comma = 0;

        struct_with_key_pos.ks_data_comma = strtol(&mesg_buf[struct_with_key_pos.ks_value_pos], nullptr, 10);

        Serial.print("data comma is: ");
        Serial.println(struct_with_key_pos.ks_data_comma);

        //struct_with_key_pos.ks_data_comma = value_from_comma; // выделенные данные присвоены полю структуры

        return struct_with_key_pos.ks_data_comma;
    }
};

void PacketOperator::spot_command_in_package(char *mesg_buf)
{                                                                                                                                                                            // найти номер команды, которая лежит в посылке
    if (struct_with_key_pos.ks_command_pos != 0 && struct_with_key_pos.ks_terminator_pos != 0 && struct_with_key_pos.ks_terminator_pos > struct_with_key_pos.ks_command_pos) // если в посылке присутствует заголовок и следующий за ним терминатор
    {
        struct_with_key_pos.comma_finded = false;
        // проверка на совпадение команды из списка массивов char
        for (uint8_t i = 0; i < counter_commands; i++) // цикл идет по всем командам // sizeof(header_comma) / sizeof_comma - колво строк в массиве (колво комманд)
        // for (uint8_t i = 0; i < sizeof(header_comma) / sizeof_comma; i++) // цикл идет по всем командам // sizeof(header_comma) / sizeof_comma - колво строк в массиве (колво комманд)
        {

            if (strncmp(*(storage_comma_pointers + i),
                        &mesg_buf[struct_with_key_pos.ks_command_pos],
                        strlen(*(storage_comma_pointers + i))) == 0) // если первые strlen(header_comma[i]) символов строки одинаковые
            // if (strncmp(header_comma[i], &mesg_buf[struct_with_key_pos.ks_command_pos], strlen(header_comma[i])) == 0) // если первые strlen(header_comma[i]) символов строки одинаковые
            {
                Serial.print("comma is: ");

                Serial.println(*(storage_comma_pointers + i)); // действие согласно найденной команде под номером i
                                                               // Serial.println(header_comma[i]); // действие согласно найденной команде под номером i

                struct_with_key_pos.num_comma = i; // содержит номер полученной команды
                // struct_with_key_pos.num_comma = commas(i); // содержит номер полученной команды
                struct_with_key_pos.comma_finded = true;
            }
        }
    }
    // return comma_finded;
};

void PacketOperator::fill_struct_with_key_positions(char *mesg_buf, uint8_t *counter) // true - если заголовок найден до терминатора
{
    // инициализация нулями, для избежания влияния прошлых пакетов
    struct_with_key_pos.ks_header_pos = 0;        // позиция заголовка
    struct_with_key_pos.ks_command_pos = 0;       // позиция начала команды
    struct_with_key_pos.ks_inbetweenchar_pos = 0; // позиция разделителя команды и числа
    struct_with_key_pos.ks_value_pos = 0;         // позиция начала числа в команде
    struct_with_key_pos.ks_terminator_pos = 0;    // позиция терминатора
    struct_with_key_pos.ks_data_comma = 0;        //число пришедшее с командой
    struct_with_key_pos.num_comma = 0;            // номер команды из enum в пакете
    struct_with_key_pos.comma_finded = false;

    for (uint8_t i = 0; i < *counter; i++) // цикл идет по всем элементам полученного массива
    {
        char in = mesg_buf[i]; // чтение очередного символа из массива

        if (in == main_param.mpp_header) // начало пакета
        {
            // command_pos = i + 1;

            struct_with_key_pos.ks_command_pos = i + 1;

            Serial.print("fill struct command pos");
            Serial.println(struct_with_key_pos.ks_command_pos);
        }

        if (in == main_param.mpp_inbetweenchar) // начало числового значения
        {
            // value_pos = i + 1;

            struct_with_key_pos.ks_value_pos = i + 1;

            Serial.print("fill struct value pos");
            Serial.println(struct_with_key_pos.ks_value_pos);
        }

        if (in == main_param.mpp_terminator_pos) // завершение пакета
        {

            mesg_buf[i] = '\0'; // терминатор заменен на символ конца строки для atoi() - преобразование str в int

            struct_with_key_pos.ks_terminator_pos = i;
            Serial.print("fill struct terminator pos");
            Serial.println(struct_with_key_pos.ks_terminator_pos);
        }
    }
}

bool PacketOperator::attach_comma(char const *ptr_mass_char)
{
    // static uint8_t counter_commands = 0; // счетчик количества команд
    // counter_commands++;

    char const **storage_pointers_new = nullptr; // промежуточный буфер

    storage_pointers_new = new char const *[counter_commands]; // выделение промежуточного буфера для хранения указателя команд

    if (storage_pointers_new == nullptr)
    { // если память больше не выделилась
        Serial.println(counter_commands);
        return false;
    }

    memset(storage_pointers_new, 0, (sizeof(char const *) * counter_commands)); // заполнение нулями

    memmove(storage_pointers_new, storage_comma_pointers, (sizeof(char const *) * counter_commands)); // перемещение указателей из основного буфера в промежуточный
    delete[] storage_comma_pointers;                                                                  // освобождение памяти основного буфера

    *(storage_pointers_new + (counter_commands - 1)) = ptr_mass_char; // выбор ячейки и передача в нее указателя на команду

    storage_comma_pointers = storage_pointers_new; // теперь промежуточный буфер - это основной буфер с командами
    return true;
};

void PacketOperator:: cout_struct_with_key_pos()
{

    Serial.print("ks_header_pos: ");    
    Serial.println(struct_with_key_pos.ks_header_pos);

    Serial.print("ks_command_pos: ");    
    Serial.println(struct_with_key_pos.ks_command_pos);

    Serial.print("ks_inbetweenchar_pos: "); 
    Serial.println(struct_with_key_pos.ks_inbetweenchar_pos);

    Serial.print("ks_value_pos: "); 
    Serial.println(struct_with_key_pos.ks_value_pos);

    Serial.print("ks_terminator_pos: "); 
    Serial.println(struct_with_key_pos.ks_terminator_pos);

    Serial.print("ks_data_comma: "); 
    Serial.println(struct_with_key_pos.ks_data_comma);

    Serial.print("num_comma: "); 
    Serial.println(struct_with_key_pos.num_comma);

}

void PacketOperator::add_comma(void (*func)(uint32_t), char const *ptr_mass_char)
{
    counter_commands++; // подсчет добавленных команд

    attach_comma(ptr_mass_char); // добавить команду
    attach_func(func);           // добавить функцию по команде
};

void PacketOperator::add_comma(void (*func)(), char const *ptr_mass_char)
{
    counter_commands++; // подсчет добавленных команд

    attach_comma(ptr_mass_char); // добавить команду
    attach_func(func);           // добавить функцию по команде
};

void PacketOperator::help()
{
    cout_commas();
}

void PacketOperator::do_attached_func()
{
    for (uint8_t i = 0; i < counter_commands; i++)
    {                                                                         // выполнение по указателям всех записанных команд
                                                                              //*(storage_attached_func_pointers + i)();// разыменовать и выполнить
        storage_attached_func_pointers[i](struct_with_key_pos.ks_data_comma); // разыменовать и выполнить
        // storage_attached_func_pointers[i]();         // разыменовать и выполнить
    }
};

void PacketOperator::do_func()
{

    storage_attached_func_pointers[struct_with_key_pos.num_comma](struct_with_key_pos.ks_data_comma); // разыменовать и выполнить
    struct_with_key_pos.num_comma = 0;                                                                // storage_attached_func_pointers[i](struct_with_key_pos.ks_data_comma); // разыменовать и выполнить
}

void PacketOperator::cout_commas()
{
    Serial.println("");
    Serial.println("help:");
    for (uint8_t i = 0; i < counter_commands; i++)
    { // вывод по указателям всех записанных команд

        Serial.print("comma ");
        Serial.print(i);
        Serial.print(": ");
        Serial.println(*(storage_comma_pointers + i));
    }
    Serial.println("");
};

// бэкап для функций, ничего не принимающих в качестве параметра
bool PacketOperator::attach_func(void (*func)()) // принять указатель на функцию
{
    // void (**storage_attached_func_pointers_new)(); // промежуточный буфер

    using fptr = void (*)();
    fptr *storage_attached_func_pointers_new = new fptr[counter_commands]; // выделение промежуточного буфера для хранения указателя команд

    if (storage_attached_func_pointers_new == nullptr)
    { // если память больше не выделилась
        Serial.println(counter_commands);
        return false;
    }

    memset(storage_attached_func_pointers_new, 0, (sizeof(func) * counter_commands)); // заполнение нулями

    memmove(storage_attached_func_pointers_new, storage_attached_func_pointers, (sizeof(func) * counter_commands)); // перемещение указателей из основного буфера в промежуточный
    delete[] storage_attached_func_pointers;                                                                        // освобождение памяти основного буфера

    *(storage_attached_func_pointers_new + (counter_commands - 1)) = func; // выбор ячейки и передача в нее указателя на команду

	//storage_attached_func_pointers = storage_attached_func_pointers_new; // теперь промежуточный буфер - это основной буфер с командами
    return true;
};

bool PacketOperator::attach_func(void (*func)(uint32_t)) // принять указатель на функцию принимающую в качестве параметра команду выделенную из посылки
{
    // void (**storage_attached_func_pointers_new)(); // промежуточный буфер

    using fptr = void (*)(uint32_t);
    fptr *storage_attached_func_pointers_new = new fptr[counter_commands]; // выделение промежуточного буфера для хранения указателя команд

    if (storage_attached_func_pointers_new == nullptr)
    { // если память больше не выделилась
        Serial.println(counter_commands);
        return false;
    }

    memset(storage_attached_func_pointers_new, 0, (sizeof(func) * counter_commands)); // заполнение нулями

    memmove(storage_attached_func_pointers_new, storage_attached_func_pointers, (sizeof(func) * counter_commands)); // перемещение указателей из основного буфера в промежуточный
    delete[] storage_attached_func_pointers;                                                                        // освобождение памяти основного буфера

    *(storage_attached_func_pointers_new + (counter_commands - 1)) = func; // выбор ячейки и передача в нее указателя на команду

    storage_attached_func_pointers = storage_attached_func_pointers_new; // теперь промежуточный буфер - это основной буфер с командами
    return true;
};

PacketOperator::PacketOperator(char header, char inbetween, char terminator)
{
    storage_comma_pointers = nullptr;
    counter_commands = 0;
    /*
    uint8_t ks_header;            // позиция заголовка
    uint8_t ks_command_pos;       // позиция начала команды
    uint8_t ks_inbetweenchar_pos; // позиция начала команды
    uint8_t ks_value_pos;         // позиция начала числа в команде
    uint8_t ks_data_comma;        //число пришедшее с командой
    uint8_t ks_terminator_pos;    // позиция терминатора
    */

/*
    //инициализация настроек для парсинга пакета
    main_param.mpp_header = '$';
    main_param.mpp_inbetweenchar = '_';
    main_param.mpp_terminator_pos = ';';
*/

    //инициализация настроек для парсинга пакета
    main_param.mpp_header = header;
    main_param.mpp_inbetweenchar = inbetween;
    main_param.mpp_terminator_pos = terminator;

    struct_with_key_pos = {0}; // заполнение полей структуры нулями
};