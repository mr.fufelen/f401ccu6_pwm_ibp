#pragma once

#include <stdlib.h>

#include "my_serial/Serial.h"
#include "string.h"
#include "program.h"
#include "cmsis_os.h"


extern TIM_HandleTypeDef htim1;

extern UART_HandleTypeDef huart1;

extern QueueHandle_t uart_queue;

#define STACK_SIZE_UART_HANDLER 256